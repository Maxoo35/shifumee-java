package shifumee;

import org.junit.Assert;
import org.junit.Test;

import shifumee.Game;
import shifumee.MainClass;


public final class TestGame {

	@Test
	public void cisorsVsRock(){
		Game yourGame = Game.CISORS;
		Game botGame = Game.ROCK;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 0);
	}
	
	@Test
	public void cisorsVsPaper(){
		Game yourGame = Game.CISORS;
		Game botGame = Game.PAPER;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 2);
	}
	
	@Test
	public void cisorsVsCisors(){
		Game yourGame = Game.CISORS;
		Game botGame = Game.CISORS;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 1);
	}
	
	@Test
	public void rockVsCisors(){
		Game yourGame = Game.ROCK;
		Game botGame = Game.CISORS;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 2);
	}
	
	@Test
	public void rockVsPaper(){
		Game yourGame = Game.ROCK;
		Game botGame = Game.PAPER;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 0);
	}
	
	@Test
	public void rockVsRock(){
		Game yourGame = Game.ROCK;
		Game botGame = Game.ROCK;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 1);
	}
	
	@Test
	public void paperVsCisors(){
		Game yourGame = Game.PAPER;
		Game botGame = Game.CISORS;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 0);
	}
	
	@Test
	public void paperVsRock(){
		Game yourGame = Game.PAPER;
		Game botGame = Game.ROCK;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 2);
	}
	
	@Test
	public void paperVsPaper(){
		Game yourGame = Game.ROCK;
		Game botGame = Game.ROCK;
		
		MainClass main = new MainClass();
		Assert.assertNotNull(main);
		
		int r = main.gameCompare(yourGame, botGame);
		Assert.assertEquals(r, 1);
	}

}
