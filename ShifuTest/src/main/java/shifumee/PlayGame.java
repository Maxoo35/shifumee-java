package shifumee;

import java.util.Scanner;
/**
 * 
 * @author maxoo
 * 
 */
public class PlayGame {
	/** is the constant for 0.*/
	private static final int ZERO = 0;
	/** is the constant for 1.*/
	private static final int ONE = 1;
	/** is the constant for 2. */
	private static final int TWO = 2;
	/** is the constant for 3. */
	private static final int THREE = 3;
	
	/** check if your choice number is OK.*/
	private boolean isOK = false;
	/** is the bot choice number.*/
	private int botGameNum;
	/** is your choice number.*/
	private int yourGameNum;
	/** for keyboard entries.*/
	private Scanner sc = new Scanner(System.in);
	/** is your game choice.*/
	private Game yourGame = null;
	/** is the bot game choice.*/
	private Game botGame = null;
	/** make reference to the MainClass.*/
	private MainClass main = new MainClass();
	/** contains the result of the game.*/
	private int result;
	
	/**
	 * The empty constructor.
	 */
	public PlayGame() { }
	
	/**
	 * Method which runs the game as much time we decide.
	 * @param roundNumber
	 */
	public void runGame(int roundNumber) {
		int round = 1;
		while(round <= roundNumber){
			System.out.println("ROUND " + round);
			isOK = false;
			while (!isOK) {
				System.out.println("Que voulez vous jouer:");
				System.out.println("1: Pierre\n2: Feuille\n3: Ciseau");
				try {
					yourGameNum = Integer.parseInt(sc.nextLine());
					isOK = true;
				} catch (Exception e) {
					System.out.println("Veuillez saisir une valeur acceptable");
				}
			}
			switch (yourGameNum) {
				case ONE:
					yourGame = Game.ROCK;
					System.out.print("Pierre");
					break;
				case TWO:
					yourGame = Game.PAPER;
					System.out.print("Papier");
					break;
				case THREE:
					yourGame = Game.CISORS;
					System.out.print("Ciseaux");
					break;
				default:
					break;
			}
			
			System.out.print(" vs ");
			
			botGameNum = main.getRandomGame();
			switch (botGameNum) {
				case ZERO:
					botGame = Game.ROCK;
					System.out.println("Pierre");
					break;
				case ONE:
					botGame = Game.PAPER;
					System.out.println("Papier");
					break;
				case TWO:
					botGame = Game.CISORS;
					System.out.println("Ciseaux");
					break;
				default:
					break;
			}
			
			result = main.gameCompare(yourGame, botGame);
			switch (result) {
				case 0:
					System.out.println("Vous avez perdu...");
					break;
				case 1:
					System.out.println("Match nul.");
					break;
				case 2:
					System.out.println("Vous avez gagné!!!");
					break;
				default:
					break;
			}
			round++;
		}
		sc.close();
	}
}
