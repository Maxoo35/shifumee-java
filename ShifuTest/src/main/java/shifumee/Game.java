package shifumee;

/**
 * 
 * @author maxoo
 * Choices enumeration for game.
 */
public enum Game {
	/** is the rock choice.*/
	ROCK,
	/** is the paper choice.*/
	PAPER,
	/** is the cisors choice.*/
	CISORS;
}
