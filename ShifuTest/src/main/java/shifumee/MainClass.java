package shifumee;

import java.util.Random;

/**
 * 
 * @author maxoo
 *
 */
public final class MainClass {
	/**is the limit choice number.*/
	public static final int LIMITCHOICE = 3;
	
	/**
	 * @param args is the default parameter of the MainClass
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PlayGame myGame = new PlayGame();
		myGame.runGame(3);
	}
	
	
	/**
	 * Compare the results of the two choices and give the result.
	 * @param yourGame is your choice
	 * @param botGame is the bot choice
	 * @return the victory or the equality or the defailt
	 */
	public int gameCompare(Game yourGame, Game botGame) {
		int result = 0;
		if (yourGame == Game.CISORS && botGame == Game.ROCK) {
			result = 0;
		}
		if (yourGame == Game.CISORS && botGame == Game.PAPER) {
			result = 2;
		}
		if (yourGame == Game.CISORS && botGame == Game.CISORS) {
			result = 1;
		}
		if (yourGame == Game.PAPER && botGame == Game.ROCK) {
			result = 2;
		}
		if (yourGame == Game.PAPER && botGame == Game.CISORS) {
			result = 0;
		}
		if (yourGame == Game.PAPER && botGame == Game.PAPER) {
			result = 1;
		}
		if (yourGame == Game.ROCK && botGame == Game.CISORS) {
			result = 2;
		}
		if (yourGame == Game.ROCK && botGame == Game.PAPER) {
			result = 0;
		}
		if (yourGame == Game.ROCK && botGame == Game.ROCK) {
			result = 1;
		}
		return result;
	}
	
	/**
	 * 
	 * @return a random number
	 */
	public int getRandomGame() {
		Random rand = new Random();
		
		int randomGame = rand.nextInt(LIMITCHOICE - 0);
		return randomGame;
	}
	
}
